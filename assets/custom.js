/*-----------------------------------------------------------------------------/
/ Custom Theme JS
/-----------------------------------------------------------------------------*/

// Insert any custom theme js here...

$(document).ready(function() {
  
  productGridSwatch();

})

const productGridSwatch = function() {
  $(document).on('click', '[data-swatch-expand]', function() {
    $(this).closest('.product-options-wrapper').addClass('expanded');
  })
  $(document).on('click', '.form-close', function(e) {
    $(this).closest('.product-options-wrapper').removeClass('expanded');
  })
  $('.product-options-wrapper form .product-swatch input').on('change', function() {
    var $button = $(this).closest('form').find('button[type="submit"]');
    if($(this).attr('data-available') == 'true') {
      $button.removeClass('disabled');
      $button.removeAttr('disabled');
    } else {
      $button.addClass('disabled');
      $button.attr('disabled', 'disabled');
    }
  })
}